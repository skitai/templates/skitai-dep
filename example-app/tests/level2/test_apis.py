import pytest
import os
from xml.dom.minidom import parseString as parse_xml

def test_index (axios, requests):
    r = axios.get ('/')
    assert r.status_code == 200

    if os.getenv ("SERVICE_STAGE") == "DEVEL":
        r = requests.get ('/static/')
        assert r.status_code == 200
