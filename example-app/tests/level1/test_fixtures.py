from rs4.webkit import jwt
import conftest
import pytest

def test_clients (requests, axios, siesta):
    r = requests.get ("/")
    assert r.status_code == 200

    r = axios.get ("/")
    assert r.status_code == 200

    r = siesta.get ()
    assert r.status_code == 200
