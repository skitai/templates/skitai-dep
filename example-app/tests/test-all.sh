#! /bin/bash
set -xe

if [[ -z "$DOCKER_VERSION" ]]
then
    pytest --disable-pytest-warnings -x level1
    pytest --disable-pytest-warnings -x level2
fi
