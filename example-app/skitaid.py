#! /usr/bin/env python3

import os
import sys
import skitai

os.environ ['ADMIN_TITLE'] = 'System'
os.environ ['SECRET_KEY'] = 'VW5IxpK0ROmwASSgKYhf2Epc5cP7H9FZ800SrZ6HkMA'

if __name__ == '__main__':
    import app

    options = skitai.get_options ()
    with skitai.preference () as pref:
        pref.config.MAX_UPLOAD_SIZE = 16 * 1024 * 1024 * 1024
        pref.set_static ('/static', 'app/static')
        skitai.mount ('/', app, pref)

    skitai.set_media ('/media')
    skitai.run (ip = '0.0.0.0', name = 'ecsdep')
