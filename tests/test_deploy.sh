#! /bin/bash

set -xe

cd ../example-app/dep

echo "Creating ECS cluster..."
ecsdep cluster create
ecsdep -f compose.ecs.novpc.yml cluster create
ecsdep -f compose.ecs.noec2.yml cluster create

export SERVICE_STAGE=production
echo "Deploy maintain service on FARGATE..."
ecsdep -f compose.maintain.yml service --latest up


export SERVICE_STAGE=qa
echo "Deploy QA service ..."
ecsdep service --latest up

export SERVICE_STAGE=production
echo "Deploy worker service ..."
ecsdep -f compose.worker.yml service --latest up

echo "Deploy production service ..."
ecsdep service --latest up
ecsdep -f compose.ecs.novpc.yml service --latest up
ecsdep -f compose.ecs.noec2.yml service --latest up
