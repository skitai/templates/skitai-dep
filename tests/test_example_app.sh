#! /bin/sh
set -xe

cd ../example-app/dep

export SERVICE_STAGE=devel
docker-compose -f compose.maintain.yml down || echo "fail"
docker-compose -f compose.ecs.yml down || echo "fail"

docker-compose -f compose.maintain.yml up -d --build
docker exec -t skitai-maintain wait-for-it.sh localhost:80 -t 30
docker exec -t skitai-maintain curl -sfo /dev/null http://localhost/ping
docker exec -t skitai-maintain curl -sfo /dev/null http://localhost/ && exit 1
docker exec -t skitai-maintain curl -sfo /dev/null http://localhost/robots.txt && exit 1
docker-compose -f compose.maintain.yml down || echo "fail"

export SERVICE_STAGE=ci
docker-compose -f compose.ecs.yml up -d --build
docker exec -t skitai-app wait-for-it.sh localhost:5000 -t 30
docker exec -t skitai-nginx wait-for-it.sh localhost:80 -t 30
docker exec -t skitai-nginx curl -sfo /dev/null http://localhost/
docker exec -t skitai-nginx curl -sfo /dev/null http://localhost/static/
docker exec -t skitai-nginx curl -sfo /dev/null http://localhost/robots.txt
docker exec -t skitai-nginx curl -sfo /dev/null http://localhost/ping
docker exec -t skitai-app pytest level1
docker exec -t skitai-app pytest level2
docker-compose -f compose.ecs.yml down || echo "fail"

export SERVICE_STAGE=qa
docker-compose -f compose.ecs.yml up -d --build
docker exec -t skitai-app wait-for-it.sh localhost:5000 -t 30
docker exec -t skitai-nginx wait-for-it.sh localhost:80 -t 30
docker exec -t skitai-nginx curl -sfo /dev/null http://localhost/
docker-compose -f compose.ecs.yml down || echo "fail"

if [ "$1" == "--push" ]
then
    docker-compose -f compose.maintain.yml push
    docker-compose -f compose.ecs.yml push
fi