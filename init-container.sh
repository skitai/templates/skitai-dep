adduser --disabled-password --shell /bin/bash --gecos "ubuntu" ubuntu

docker login -u hansroh -p "$DOCKER_PASSWORD"
docker login -u hansroh -p "$GITLAB_TOKEN" registry.gitlab.com

git config --global --add safe.directory /app
git config user.email "hansroh@gmail.com"
git config user.name "hansroh"
git config pull.rebase true

pip3 install -e .
pip3 install pytest

alias ll='ls -al'
