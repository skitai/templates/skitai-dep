apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/3bf863cc.pub
apt install -y build-essential zlib1g-dev cmake vim git tmux
apt install -y python3-dev software-properties-common
pip3 install -U pip