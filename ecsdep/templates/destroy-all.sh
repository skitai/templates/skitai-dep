cd task-def
terraform workspace select production
terraform destroy -auto-approve
terraform workspace select default
terraform destroy -auto-approve

cd ../ecs-cluster
terraform destroy -auto-approve
